<?php

namespace Database\Seeders;

use App\Models\GuestBook;
use Illuminate\Database\Seeder;

class GuestBookSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        $faker = \Faker\Factory::create('id_ID');
        foreach (range(1, 1000) as $i) {
            array_push($data, [
                'full_name'    => $faker->name,
                'phone_number' => $faker->phoneNumber,
                'email'        => $faker->email,
                'agency'       => $faker->company,
                'meet_with'    => $faker->name,
                'goal'         => $faker->text(100),
                'attachment'   => $faker->imageUrl,
                'created_at'   => $faker->dateTimeBetween('-1 year', 'now'),
                'updated_at'   => now(),
            ]);
        }
        foreach (collect($data)->chunk(20) as $chunkData) {
            GuestBook::insert($chunkData->toArray());
        }
    }
}
