@php
  $isAjax = $isAjax ?? false;
  $isAjaxYajra = $isAjaxYajra ?? false;
@endphp

@extends('stisla.layouts.app-datatable')

@section('table')
  @include('stisla.guest-books.table')
@endsection

@section('filter')
  @include('stisla.guest-books.filter')
@endsection
