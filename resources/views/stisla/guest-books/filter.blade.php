<div class="row">
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4><i class="fa fa-filter"></i> Filter Tanggal</h4>
        <div class="card-header-action">
        </div>
      </div>
      <div class="card-body">

        <form action="">
          <input type="hidden" name="type" value="1">
          @csrf
          <div class="row">
            <div class="col-md-6">
              @include('stisla.includes.forms.inputs.input', [
                  'type' => 'date',
                  'id' => 'from_date',
                  'required' => true,
                  'label' => __('Dari Tanggal'),
                  'value' => request('from_date', date('Y-m-d', strtotime('-1 month'))),
              ])
            </div>
            <div class="col-md-6">
              @include('stisla.includes.forms.inputs.input', [
                  'type' => 'date',
                  'id' => 'to_date',
                  'required' => true,
                  'label' => __('Ke Tanggal'),
                  'value' => request('to_date', date('Y-m-d')),
              ])
            </div>
            {{-- @if ($isSuperAdmin)
            <div class="col-md-3">
              @include('stisla.includes.forms.selects.select2', [
                  'id' => 'filter_user',
                  'name' => 'filter_user',
                  'label' => __('Pilih Pengguna'),
                  'options' => $users,
                  'selected' => request('filter_user'),
                  'with_all' => true,
              ])
            </div>
            <div class="col-md-3">
              @include('stisla.includes.forms.selects.select2', [
                  'id' => 'filter_role',
                  'name' => 'filter_role',
                  'label' => __('Pilih Role'),
                  'options' => $roles,
                  'selected' => request('filter_role'),
                  'with_all' => true,
              ])
            </div>

            <div class="col-md-3">
              @include('stisla.includes.forms.selects.select2', [
                  'id' => 'filter_kind',
                  'name' => 'filter_kind',
                  'label' => __('Pilih Jenis Aktivitas'),
                  'options' => $kinds,
                  'selected' => request('filter_kind'),
                  'with_all' => true,
              ])
            </div>
          @endif
          @if (count($deviceOptions) > 0)
            <div class="col-md-3">
              @include('stisla.includes.forms.selects.select2', [
                  'id' => 'filter_device',
                  'name' => 'filter_device',
                  'label' => __('Pilih Device'),
                  'options' => $deviceOptions,
                  'selected' => request('filter_device'),
                  'with_all' => true,
              ])
            </div>
          @endif
          @if (count($platformOptions) > 0)
            <div class="col-md-3">
              @include('stisla.includes.forms.selects.select2', [
                  'id' => 'filter_platform',
                  'name' => 'filter_platform',
                  'label' => __('Pilih Platform'),
                  'options' => $platformOptions,
                  'selected' => request('filter_platform'),
                  'with_all' => true,
              ])
            </div>
          @endif
          @if (count($browserOptions) > 0)
            <div class="col-md-3">
              @include('stisla.includes.forms.selects.select2', [
                  'id' => 'filter_browser',
                  'name' => 'filter_browser',
                  'label' => __('Pilih Browser'),
                  'options' => $browserOptions,
                  'selected' => request('filter_browser'),
                  'with_all' => true,
              ])
            </div>
          @endif --}}
          </div>
          <button class="btn btn-primary icon"><i class="fa fa-search"></i> Cari Data</button>
        </form>
      </div>
    </div>
  </div>
  <div class="col-md-6">
    <div class="card">
      <div class="card-header">
        <h4><i class="fa fa-filter"></i> Filter Waktu</h4>
        <div class="card-header-action">
        </div>
      </div>
      <div class="card-body">

        <form action="">
          <input type="hidden" name="type" value="2">
          @csrf
          <div class="row">
            <div class="col-md-6">
              @include('stisla.includes.forms.selects.select', [
                  'id' => 'filter_month',
                  'name' => 'filter_month',
                  'label' => __('Pilih Bulan'),
                  'options' => array_bulan(),
                  'selected' => request('filter_month'),
                  'with_all' => false,
              ])
            </div>
            <div class="col-md-6">
              @include('stisla.includes.forms.selects.select', [
                  'id' => 'filter_year',
                  'name' => 'filter_year',
                  'label' => __('Pilih Tahun'),
                  'options' => array_year(2019, date('Y')),
                  'selected' => request('filter_year'),
                  'with_all' => false,
              ])
            </div>
          </div>
          <button class="btn btn-primary icon"><i class="fa fa-search"></i> Cari Data</button>
        </form>
      </div>
    </div>
  </div>
</div>

@if (request('type') == 2)
  <div class="alert alert-info">
    Menampilkan data pada {{ array_bulan()[request('filter_month')] }} {{ request('filter_year') }}
  </div>
@endif
@if (request('type') == 1)
  <div class="alert alert-info">
    Menampilkan data pada {{ request('from_date') }} sampai {{ request('to_date') }}
  </div>
@endif
@if (!request('type'))
  <div class="alert alert-info">
    Silakan gunakan filter di atas
  </div>
@endif
