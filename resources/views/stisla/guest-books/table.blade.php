@php
  $isExport = $isExport ?? false;
  $isAjax = $isAjax ?? false;
  $isYajra = $isYajra ?? false;
  $isAjaxYajra = $isAjaxYajra ?? false;
@endphp

<table class="table table-striped @if ($isYajra || $isAjaxYajra) yajra-datatable @endif"
  @if ($isYajra || $isAjaxYajra) data-ajax-url="{{ $routeYajra }}?isAjaxYajra={{ $isAjaxYajra }}" @else  id="datatable" @endif
  @if ($isExport === false && $canExport) data-export="true" data-title="{{ __('Buku Tamu') }}" @endif>
  <thead>
    <tr>
      @if ($isExport)
        <th class="text-center">#</th>
      @else
        <th>{{ __('No') }}</th>
      @endif
      <th>{{ __('Nama Lengkap') }}</th>
      <th>{{ __('No HP') }}</th>
      <th>{{ __('Email') }}</th>
      <th>{{ __('Instansi') }}</th>
      <th>{{ __('Bertemu Dengan') }}</th>
      <th>{{ __('Keperluan') }}</th>
      @if (!$isExport)
        <th>{{ __('Foto') }}</th>
      @endif
      @if ($isExport)
        {{-- <th>{{ __('Summernote Simple') }}</th>
        <th>{{ __('Summernote') }}</th> --}}
      @endif
      <th>{{ __('Created At') }}</th>
      <th>{{ __('Updated At') }}</th>
      @if ($isExport === false && ($canUpdate || $canDelete || $canDetail))
        <th>{{ __('Aksi') }}</th>
      @endif
    </tr>
  </thead>
  <tbody>
    @if ($isYajra === false)
      @foreach ($data as $item)
        <tr>
          <td>{{ $loop->iteration }}</td>
          <td>{{ $item->full_name }}</td>
          <td>{{ $item->phone_number }}</td>
          <td>{{ $item->email }}</td>
          {{-- <td>{{ dollar($item->currency) }}</td>
          <td>{{ rp($item->currency_idr) }}</td> --}}
          <td>{{ $item->agency }}</td>
          <td>{{ $item->meet_with }}</td>
          <td>{{ $item->goal }}</td>
          {{-- <td>
            {{ is_array($item->select2_multiple) ? implode(', ', $item->select2_multiple) : $item->select2_multiple }}
          </td> --}}
          @if (!$isExport)
            <td>
              @if ($item->attachment)
                <a href="{{ $urlLink = $item->attachment }}" target="_blank">
                  <img src="{{ $urlLink }}" alt="{{ $item->attachment }}" width="100">
                </a>
              @else
                -
              @endif
            </td>
          @endif
          {{-- <td>{{ is_array($item->checkbox) ? implode(', ', $item->checkbox) : $item->checkbox }}</td>
          <td>{{ is_array($item->checkbox2) ? implode(', ', $item->checkbox2) : $item->checkbox2 }}</td> --}}

          {{-- @if ($isExport === false)
            <td>
              @include('stisla.crud-examples.tags', ['tags' => $item->tags])
            </td>
          @else
            <td>{{ implode(', ', explode(',', $item->tags)) }}</td>
          @endif --}}

          {{-- @if ($isExport)
            <td>
              @if (Str::contains($item->file, 'http'))
                <a href="{{ $item->file }}">{{ $item->file }}</a>
              @elseif($item->file)
                <a href="{{ $urlLink = Storage::url('crud-examples/' . $item->file) }}">{{ $urlLink }}</a>
              @else
                -
              @endif
            </td>
          @else
            <td>
              @include('stisla.crud-examples.file', ['file' => $item->file])
            </td>
          @endif --}}

          {{-- <td>
            @include('stisla.crud-examples.color', ['color' => $item->color])
          </td>

          @if ($isExport)
            <td>{{ $item->summernote_simple }}</td>
            <td>{{ $item->summernote }}</td>
          @endif --}}

          <td>{{ $item->created_at }}</td>
          <td>{{ $item->updated_at }}</td>

          @if ($isExport === false)
            @include('stisla.includes.forms.buttons.btn-action')
          @endif
        </tr>
      @endforeach
    @endif
  </tbody>
</table>
