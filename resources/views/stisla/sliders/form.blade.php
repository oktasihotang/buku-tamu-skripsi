@php
  $isAjax = $isAjax ?? false;
@endphp

@extends('stisla.layouts.app-form')

@section('rowForm')
  @include('stisla.sliders.only-form')
@endsection

@push('css')
@endpush

@push('js')
@endpush

@push('scripts')
@endpush
