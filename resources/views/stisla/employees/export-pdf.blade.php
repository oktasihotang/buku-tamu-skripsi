<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Data Pegawai</title>

  @include('stisla.includes.others.css-pdf')
</head>

<body>
  <h1>Data Pegawai</h1>
  @include('stisla.employees.table')
</body>

</html>
