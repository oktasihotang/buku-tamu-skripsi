@extends('stisla.layouts.app-auth-simple', ['customContainer' => 'col-12 col-sm-8 offset-sm-2 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2'])

@section('title')
  {{ $title = __('Buku Tamu Instansi Pemerintah XYZ') }}
@endsection

@section('slider')
  @if ($sliders->count() > 0)
    <!-- Slideshow container -->
    <div class="slideshow-container">

      <!-- Full-width slides/quotes -->
      @foreach ($sliders as $item)
        <div class="mySlides">
          <q>{{ $item->text }}</q>
          {{-- <p class="author">- John Keats</p> --}}
        </div>
      @endforeach

      <!-- Next/prev buttons -->
      <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
      <a class="next" onclick="plusSlides(1)">&#10095;</a>
    </div>

    <!-- Dots/bullets/indicators -->
    <div class="dot-container">
      @foreach ($sliders as $item)
        <span class="dot" onclick="currentSlide({{ $loop->iteration }})"></span>
      @endforeach
    </div>
  @endif
@endsection

@section('content')
  <div class="card-body">

    <form method="POST" action="" class="needs-validation" novalidate="" enctype="multipart/form-data" onsubmit="onSubmit(event)">
      @csrf
      <div class="row">
        <div class="col-md-6">
          @include('stisla.includes.forms.inputs.input', ['id' => 'full_name', 'name' => 'full_name', 'label' => __('Nama Lengkap'), 'required' => true, 'icon' => 'fa fa-user'])
        </div>
        <div class="col-md-6">
          @include('stisla.includes.forms.inputs.input-email')
        </div>
        <div class="col-md-6">
          @include('stisla.includes.forms.inputs.input', ['id' => 'phone_number', 'name' => 'phone_number', 'label' => __('No HP'), 'required' => true, 'icon' => 'fa fa-phone'])
        </div>
        <div class="col-md-6">
          @include('stisla.includes.forms.inputs.input', ['id' => 'agency', 'name' => 'agency', 'label' => __('Instansi'), 'required' => true, 'icon' => 'fa fa-university'])
        </div>
        <div class="col-md-6">
          @include('stisla.includes.forms.inputs.input', ['id' => 'meet_with', 'name' => 'meet_with', 'label' => __('Bertemu Dengan'), 'required' => true, 'icon' => 'fa fa-user-plus'])
        </div>
        <div class="col-md-6">
		  @if ($disabled ?? false)
            @include('stisla.includes.forms.inputs.input', ['id' => 'goal', 'name' => 'goal', 'label' => __('Keperluan'), 'required' => true, 'icon' => 'fa fa-check'])
          @else
            <div class="form-group">
              <label for="goal" class="form-label">{{ __('Keperluan') }} <span class="text-danger">*</span></label>
              <select name="goal" id="goal" class="form-control select2" required onchange="goalOnChange(event)">
                <option value="">{{ __('Pilih Keperluan') }}</option>
                @foreach ($goals as $item)
                  <option value="{{ $item->goal_name }}">{{ $item->goal_name }}</option>
                @endforeach
                <option value="lainnya">Lainnya</option>
              </select>
            </div>
          @endif

        </div>
        <div class="col-md-6" id="goalArea">
          @include('stisla.includes.forms.inputs.input', ['id' => 'other', 'name' => 'other', 'label' => __('Keperluan Lainnya'), 'required' => false, 'icon' => 'fa fa-bookmark'])
        </div>
        @if ($disabled ?? false)
          <div class="col-12" align="center">
            <strong>Foto</strong>
            <br>
            <img src="{{ $d->attachment }}" alt="{{ $d->attachment }}" class="img-thumbnail">
            <br>
            <br>
            <hr>
          </div>
          <?php /*?>
	      <div class="col-12" align="center">
            <strong>Scan QR Code di bawah untuk mempermudah mengisi buku tamu lagi</strong>
            <br>
            <br>
            <img src="{{ $_qr_code }}" alt="{{ $_qr_code }}" class="img-thumbnail" style="max-width: 250px;">
            <br>
            <br>
          </div>
          <?php */?>
          <div class="col-12" align="center">
            <strong>Klik tombol di bawah ini untuk mengisi Buku Tamu berikutnya</strong>
            <br>
            <br>
            <a href="/insert" class="btn btn-primary btn-save-form btn-icon icon-left">Isi Buku Tamu</a>
            <br>
            <br>
          </div>
        @else
          <div class="col-12" id="areaWebCam">
            <a href="#" class="btn btn-primary" onclick="startWebCam(event)">Start Webcam</a>
            <br>
            <br>
            <video id="video" autoplay></video>
            <a id="takePictureBtn" href="#" class="btn btn-primary" onclick="takePicture(event)">Take Picture</a>
            <canvas id="canvas"></canvas>
            {{-- @include('stisla.includes.forms.inputs.input', [
                'id' => 'attachment',
                'name' => 'attachment',
                'label' => __('Foto'),
                'required' => true,
                'accept' => 'image/*',
                'type' => 'file',
            ]) --}}
            <input type="hidden" name="photo" id="photoHidden">
          </div>
        @endif
      </div>

      @if ($disabled ?? false)
      @else
        <div class="form-group">
          <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
            Simpan
          </button>
        </div>
      @endif
    </form>

  </div>
@endsection

@if (count($meets) > 0)
  @push('css')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css">
  @endpush

  @push('js')
    <script src="https://code.jquery.com/ui/1.13.2/jquery-ui.js"></script>
    <script>
      $("#meet_with").autocomplete({
        source: JSON.parse('{!! json_encode($meets) !!}')
      });
    </script>
  @endpush
@endif

@push('scripts')
  @if ($disabled ?? false)
    <script>
      $(function() {
        $('input, select, textarea').attr('disabled', true);
      })
    </script>
  @endif

  <script>
	  $(function() {
	      $('#goalArea').hide();
	    })
	
	    function goalOnChange(e) {
	      var value = e.target.value;
	      if (value === 'lainnya') {
	        $('#goalArea').show();
	      } else {
	        $('#goalArea').hide();
	      }
	    }
	
	    function onSubmit(e) {
	      if (!$('#photoHidden').val()) {
	        e.preventDefault();
	        alert('Silahkan ambil foto terlebih dahulu');
	      }
	    }

    function stopWebcam() {
      $('#stopDetection').trigger('click');
      var videoEl = document.getElementById('inputVideo');
      // now get the steam
      let stream = videoEl.srcObject;
      // now get all tracks
      let tracks = stream.getTracks();
      // now close each track by having forEach loop
      tracks.forEach(function(track) {
        // stopping every track
        track.stop();
      });
      // assign null to srcObject of video
      videoEl.srcObject = null;
      $('#areaWebcam').hide()
    }

    let camera_button = document.querySelector("#start-camera");
    let video = document.querySelector("#video");
    let click_button = document.querySelector("#click-photo");

    var width = $('#areaWebCam').width()
    var height = 200


    async function startWebCam(e) {
      e.preventDefault();
      $('#video').show()
      $('#takePictureBtn').show();
      navigator.mediaDevices.getUserMedia({
        video: true,
        audio: false
      }).then(function(stream) {
        video.srcObject = stream;
        video.setAttribute('autoplay', '');
        video.setAttribute('muted', '');
        video.setAttribute('playsinline', '')
      });
    }

    async function takePicture(e) {
      e.preventDefault();
      $('#canvas').show()
      height = $('#video').height()
      let canvas = document.querySelector("#canvas");
      $('#canvas').attr('width', width)
      $('#canvas').attr('height', height)
      canvas.getContext('2d').drawImage(video, 0, 0, width, height);
      let image_data_url = canvas.toDataURL('image/jpeg');

      // data url of the image
      console.log(image_data_url);
      $('#photoHidden').val(image_data_url)
    }

    $(function() {
      width = $('#areaWebCam').width()
      $('#video').width(width)
      $('#takePictureBtn').hide();
      $('#video').hide()
      $('#canvas').hide()
    })
  </script>

  @if (@$sliders->count() > 0)
    <script>
      var slideIndex = 1;
      showSlides(slideIndex);

      function plusSlides(n) {
        showSlides(slideIndex += n);
      }

      function currentSlide(n) {
        showSlides(slideIndex = n);
      }

      function showSlides(n) {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        var dots = document.getElementsByClassName("dot");
        if (n > slides.length) {
          slideIndex = 1
        }
        if (n < 1) {
          slideIndex = slides.length
        }
        for (i = 0; i < slides.length; i++) {
          slides[i].style.display = "none";
        }
        for (i = 0; i < dots.length; i++) {
          dots[i].className = dots[i].className.replace(" active", "");
        }
        slides[slideIndex - 1].style.display = "block";
        dots[slideIndex - 1].className += " active";
      }

      setInterval(() => {
        plusSlides(1)
      }, 5000);
    </script>
  @endif
@endpush

@if ($sliders->count() > 0)
  @push('styles')
    <style>
      /* Slideshow container */
      .slideshow-container {
        position: relative;
        background: #f1f1f1f1;
      }

      /* Slides */
      .mySlides {
        display: none;
        padding: 40px 80px;
        text-align: center;
      }

      /* Next & previous buttons */
      .prev,
      .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -30px;
        padding: 16px;
        color: #888;
        font-weight: bold;
        font-size: 20px;
        border-radius: 0 3px 3px 0;
        user-select: none;
      }

      /* Position the "next button" to the right */
      .next {
        position: absolute;
        right: 0;
        border-radius: 3px 0 0 3px;
      }

      /* On hover, add a black background color with a little bit see-through */
      .prev:hover,
      .next:hover {
        background-color: rgba(0, 0, 0, 0.8);
        color: white;
      }

      /* The dot/bullet/indicator container */
      .dot-container {
        text-align: center;
        padding: 20px;
        background: #ddd;
      }

      /* The dots/bullets/indicators */
      .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
      }

      /* Add a background color to the active dot/circle */
      .active,
      .dot:hover {
        background-color: #717171;
      }

      /* Add an italic font style to all quotes */
      q {
        font-style: italic;
      }

      /* Add a blue color to the author */
      .author {
        color: cornflowerblue;
      }
    </style>
  @endpush
@endif
