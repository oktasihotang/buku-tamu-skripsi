@extends('stisla.layouts.app-auth-simple')

@section('title')
  {{ $title = __('Isi Buku Tamu') }}
@endsection

@section('content')
  <div class="card-body">
    <form method="POST" action="" class="needs-validation" novalidate="" enctype="multipart/form-data">
      @csrf

      @include('stisla.includes.forms.inputs.input', ['id' => 'full_name', 'name' => 'full_name', 'label' => __('Nama Lengkap'), 'required' => true, 'icon' => 'fa fa-user'])
      @include('stisla.includes.forms.inputs.input-email')
      @include('stisla.includes.forms.inputs.input', ['id' => 'phone_number', 'name' => 'phone_number', 'label' => __('No HP'), 'required' => true, 'icon' => 'fa fa-phone'])
      @include('stisla.includes.forms.inputs.input', ['id' => 'agency', 'name' => 'agency', 'label' => __('Instansi'), 'required' => true, 'icon' => 'fa fa-university'])
      @include('stisla.includes.forms.inputs.input', ['id' => 'meet_with', 'name' => 'meet_with', 'label' => __('Bertemu Dengan'), 'required' => true, 'icon' => 'fa fa-user-plus'])
      @include('stisla.includes.forms.inputs.input', ['id' => 'goal', 'name' => 'goal', 'label' => __('Keperluan'), 'required' => true, 'icon' => 'fa fa-check'])
      @include('stisla.includes.forms.inputs.input', ['id' => 'attachment', 'name' => 'attachment', 'label' => __('Foto'), 'required' => true, 'accept' => 'image/*', 'type' => 'file'])
      {{-- @include('stisla.auth.login.input-password') --}}

      {{-- <div class="form-group">
        <div class="custom-control custom-checkbox">
          <input type="checkbox" name="remember" class="custom-control-input" tabindex="3" id="remember-me">
          <label class="custom-control-label" for="remember-me">{{ __('Ingat Saya') }}</label>
        </div>
      </div> --}}

      <video width="700" id="inputVideo" autoplay muted></video>


      <div class="form-group">
        <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
          {{ $title }}
        </button>
      </div>
    </form>

  </div>
@endsection

@push('scripts')
  <script>
    async function startWebcam() {
      $('#radioArea').show();
      $('#checkArea').show();
      // try to access users webcam and stream the images
      // to the video element
      const videoEl = document.getElementById('inputVideo')
      navigator.getUserMedia({
          video: {}
        },
        stream => videoEl.srcObject = stream,
        err => console.error(err)
      )
    }

    setTimeout(() => {
      startWebcam()
    }, 5000);
  </script>
@endpush
