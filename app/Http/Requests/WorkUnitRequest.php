<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WorkUnitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'work_unit_name'    => 'required',
            // 'email'        => 'required|email',
            // "phone_number" => "required",
            // "agency"       => "required",
            // "meet_with"    => "required",
            // "goal"         => "required",
            // "attachment"   => $this->isMethod('put') ? 'nullable|image' : "required|image",
        ];
    }
}
