<?php

namespace App\Http\Controllers;

use App\Exports\GuestBookExport;
use App\Http\Requests\GuestBookRequest;
use App\Http\Requests\ImportExcelRequest;
use App\Models\Slider;
use App\Imports\GuestBookImport;
use App\Models\GuestBook;
use App\Models\Goal;
use App\Repositories\GuestBookRepository;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class GuestBookController extends StislaController
{

    /**
     * crud example repository
     *
     * @var GuestBookRepository
     */
    private GuestBookRepository $guestBookRepository;

    /**
     * constructor method
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->icon                  = 'fa fa-book';
        $this->guestBookRepository = new GuestBookRepository;

        // $this->defaultMiddleware('Buku Tamu');
        $this->middleware('can:Buku Tamu')->only(['index', 'indexYajra', 'indexAjax', 'indexAjaxYajra']);
    }

    /**
     * get index data
     *
     * @return array
     */
    protected function getIndexData()
    {
        $isYajra = Route::is('guest-books.index-yajra');
        $isAjax  = Route::is('guest-books.index-ajax');
        $isAjaxYajra = Route::is('guest-books.index-ajax-yajra');
        if ($isYajra || $isAjaxYajra) {
            $data = collect([]);
        } else {
            if (request('_token'))
                $data = $this->guestBookRepository->getFullData();
            else
                $data = collect([]);
        }

        $defaultData = $this->getDefaultDataIndex(__('Buku Tamu'), 'Buku Tamu', 'guest-books');
        return array_merge($defaultData, [
            'data'         => $data,
            'isYajra'      => $isYajra,
            'isAjax'       => $isAjax,
            'isAjaxYajra'  => $isAjaxYajra,
            'yajraColumns' => $this->guestBookRepository->getYajraColumns(),
        ]);
    }

    /**
     * prepare store data
     *
     * @param GuestBookRequest $request
     * @return array
     */
    private function getStoreData(GuestBookRequest $request)
    {
        $data = $request->only([
            'full_name',
            'phone_number',
            'email',
            'agency',
            'meet_with',
            'goal',
            // 'attachment',
        ]);
        $base64 = $request->photo;
        if ($base64){
	        $pathToSave = storage_path('app/public/guest-books/attachments/' . $request->full_name . date('YmdHis') . '.jpg');
			$pathToSave = $this->fileService->base64ToJpeg($base64, 'guest-books/attachments/');
        }else{
	        $pathToSave = "https://via.placeholder.com/640x480.png/005522?text=bukutamu";
        }
        
        $data['attachment'] = $pathToSave;
        if ($request->goal === 'lainnya') {
            $data['goal'] = $request->other;
        }

        // if ($request->hasFile('attachment')) {
        //     $data['attachment'] = $this->fileService->uploadAttachmentGuestBook($request->file('attachment'));
        // }
        // $data['currency'] = str_replace(',', '', $request->currency);
        // $data['currency_idr'] = str_replace('.', '', $request->currency_idr);

        return $data;
    }

    /**
     * get detail data
     *
     * @param GuestBook $guestBook
     * @param bool $isDetail
     * @return array
     */
    private function getDetailData(GuestBook $guestBook, bool $isDetail = false)
    {
        $title       = __('Buku Tamu');
        $defaultData = $this->getDefaultDataDetail($title, 'guest-books', $guestBook, $isDetail);
        return array_merge($defaultData, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $isDetail ? __('Detail Buku Tamu') : __('Ubah Buku Tamu'),
        ]);
    }

    /**
     * get export data
     *
     * @return array
     */
    protected function getExportData(): array
    {
        $times = date('Y-m-d_H-i-s');
        $data = [
            'isExport'   => true,
            'pdf_name'   => $times . '_buku_tamu.pdf',
            'excel_name' => $times . '_buku_tamu.xlsx',
            'csv_name'   => $times . '_buku_tamu.csv',
            'json_name'  => $times . '_buku_tamu.json',
        ];
        return array_merge($this->getIndexData(), $data);
    }

    /**
     * showing crud example page
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->getIndexData();

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'data'    => view('stisla.guest-books.table', $data)->render(),
            ]);
        }

        return view('stisla.guest-books.index', $data);
    }

    /**
     * datatable yajra index
     *
     * @return Response
     */
    public function yajraAjax()
    {
        $defaultData = $this->getDefaultDataIndex(__('Buku Tamu'), 'Buku Tamu', 'guest-books');
        return $this->guestBookRepository->getYajraDataTables($defaultData);
    }

    /**
     * showing add new crud example page
     *
     * @return Response
     */
    public function create()
    {
        $title      = __('Buku Tamu');
        $fullTitle  = __('Tambah Buku Tamu');
        $data       = $this->getDefaultDataCreate($title, 'guest-books');
        $data       = array_merge($data, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $fullTitle,
        ]);
        if (request()->ajax()) {
            return view('stisla.guest-books.only-form', $data);
        }
        return view('stisla.guest-books.form', $data);
    }

    /**
     * save new crud example to db
     *
     * @param GuestBookRequest $request
     * @return Response
     */
    public function store(GuestBookRequest $request)
    {
        $data   = $this->getStoreData($request);
        $result = $this->guestBookRepository->create($data);
        logCreate("Buku Tamu", $result);
        $successMessage = successMessageCreate("Buku Tamu");

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return redirect()->route('inserted', encode_id($result->id))->with('successMessage', $successMessage);

        return back()->with('successMessage', $successMessage);
    }

    /**
     * showing edit crud example page
     *
     * @param GuestBook $guestBook
     * @return Response
     */
    public function edit(GuestBook $guestBook)
    {
        $data = $this->getDetailData($guestBook);

        if (request()->ajax()) {
            return view('stisla.guest-books.only-form', $data);
        }

        return view('stisla.guest-books.form', $data);
    }

    /**
     * update data to db
     *
     * @param GuestBookRequest $request
     * @param GuestBook $guestBook
     * @return Response
     */
    public function update(GuestBookRequest $request, GuestBook $guestBook)
    {
        $data    = $this->getStoreData($request);
        $newData = $this->guestBookRepository->update($data, $guestBook->id);
        logUpdate("Buku Tamu", $guestBook, $newData);
        $successMessage = successMessageUpdate("Buku Tamu");

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    public function show(GuestBook $guestBook)
    {
        $data = $this->getDetailData($guestBook, true);

        if (request()->ajax()) {
            return view('stisla.guest-books.only-form', $data);
        }

        return view('stisla.guest-books.form', $data);
    }

    public function inserted($id)
    {
        try {
            $guestBook = $this->guestBookRepository->find(decode_id($id));
            return view('stisla.homes.inserted', [
                'd'        => $guestBook,
                'disabled' => true,
                'meets'    => [],
                'sliders' => Slider::all(),
                'goals'    => Goal::all(),
            ]);
        } catch (Exception $e) {
            abort(404);
        }
        $data = $this->getDetailData($guestBook, true);

        if (request()->ajax()) {
            return view('stisla.guest-books.only-form', $data);
        }

        return view('stisla.guest-books.form', array_merge($data, [
            'disabled' => true
        ]));
    }

    /**
     * delete crud example from db
     *
     * @param GuestBook $guestBook
     * @return Response
     */
    public function destroy(GuestBook $guestBook)
    {
        $this->fileService->deleteAttachmentGuestBook($guestBook);
        $this->guestBookRepository->delete($guestBook->id);
        logDelete("Buku Tamu", $guestBook);
        $successMessage = successMessageDelete("Buku Tamu");

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    /**
     * download import example
     *
     * @return BinaryFileResponse
     */
    public function importExcelExample(): BinaryFileResponse
    {
        // bisa gunakan file excel langsung sebagai contoh
        // $filepath = public_path('example.xlsx');
        // return response()->download($filepath);

        $excel = new GuestBookExport($this->guestBookRepository->getLatest());
        return $this->fileService->downloadExcel($excel, 'crud_examples_import.xlsx');
    }

    /**
     * import excel file to db
     *
     * @param ImportExcelRequest $request
     * @return Response
     */
    public function importExcel(ImportExcelRequest $request)
    {
        $this->fileService->importExcel(new GuestBookImport, $request->file('import_file'));
        $successMessage = successMessageImportExcel("Buku Tamu");
        return back()->with('successMessage', $successMessage);
    }

    /**
     * download export data as json
     *
     * @return BinaryFileResponse
     */
    public function json(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadJson($data['data'], $data['json_name']);
    }

    /**
     * download export data as xlsx
     *
     * @return Response
     */
    public function excel(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadExcelGeneral('stisla.guest-books.table', $data, $data['excel_name']);
    }

    /**
     * download export data as csv
     *
     * @return Response
     */
    public function csv(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadCsvGeneral('stisla.guest-books.table', $data, $data['csv_name']);
    }

    /**
     * download export data as pdf
     *
     * @return Response
     */
    public function pdf(): Response
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadPdfA4('stisla.includes.others.export-pdf', $data, $data['pdf_name']);
    }

    public function graphic()
    {
        if (request('_token'))
            $data = $this->guestBookRepository->getFullData();
        else
            $data = collect([]);
        $data = $data->reverse()->values();
        $values = $data->groupBy('date')->map(function ($item) {
            return $item->count();
        })->values()->toArray();
        $labels = $data->pluck('date')->unique()->values()->toArray();
        return view('stisla.guest-books.graphic', [
            'values' => $values,
            'labels' => $labels,
        ]);
    }
}
