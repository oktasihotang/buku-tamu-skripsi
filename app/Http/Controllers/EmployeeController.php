<?php

namespace App\Http\Controllers;

use App\Exports\EmployeeExport;
use App\Http\Requests\EmployeeRequest;
use App\Http\Requests\ImportExcelRequest;
use App\Imports\EmployeeImport;
use App\Models\Employee;
use App\Models\WorkUnit;
use App\Repositories\EmployeeRepository;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class EmployeeController extends StislaController
{

    /**
     * crud example repository
     *
     * @var EmployeeRepository
     */
    private EmployeeRepository $employeeRepository;

    /**
     * constructor method
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->icon                  = 'fa fa-users';
        $this->employeeRepository = new EmployeeRepository;

        // $this->defaultMiddleware('Pegawai');
        $this->middleware('can:Pegawai')->only(['index', 'indexYajra', 'indexAjax', 'indexAjaxYajra']);
    }

    /**
     * get index data
     *
     * @return array
     */
    protected function getIndexData()
    {
        $isYajra = Route::is('employees.index-yajra');
        $isAjax  = Route::is('employees.index-ajax');
        $isAjaxYajra = Route::is('employees.index-ajax-yajra');
        if ($isYajra || $isAjaxYajra) {
            $data = collect([]);
        } else {
            // if (request('_token'))
            $data = $this->employeeRepository->getLatest();
            // else
            // $data = collect([]);
        }

        $defaultData = $this->getDefaultDataIndex(__('Pegawai'), 'Pegawai', 'employees');
        return array_merge($defaultData, [
            'data'         => $data,
            'isYajra'      => $isYajra,
            'isAjax'       => $isAjax,
            'isAjaxYajra'  => $isAjaxYajra,
            'yajraColumns' => $this->employeeRepository->getYajraColumns(),
        ]);
    }

    /**
     * prepare store data
     *
     * @param EmployeeRequest $request
     * @return array
     */
    private function getStoreData(EmployeeRequest $request)
    {
        $data = $request->only([
            'name',
            'work_unit_id',
            // 'phone_number',
            // 'email',
            // 'agency',
            // 'meet_with',
            // 'goal',
            // 'attachment',
        ]);
        // $base64 = $request->photo;
        // $pathToSave = storage_path('app/public/employees/attachments/' . $request->full_name . date('YmdHis') . '.jpg');
        // $pathToSave = $this->fileService->base64ToJpeg($base64, 'employees/attachments/');
        // $data['attachment'] = $pathToSave;
        // if ($request->hasFile('attachment')) {
        //     $data['attachment'] = $this->fileService->uploadAttachmentEmployee($request->file('attachment'));
        // }
        // $data['currency'] = str_replace(',', '', $request->currency);
        // $data['currency_idr'] = str_replace('.', '', $request->currency_idr);

        return $data;
    }

    /**
     * get detail data
     *
     * @param Employee $employee
     * @param bool $isDetail
     * @return array
     */
    private function getDetailData(Employee $employee, bool $isDetail = false)
    {
        $title       = __('Pegawai');
        $defaultData = $this->getDefaultDataDetail($title, 'employees', $employee, $isDetail);
        return array_merge($defaultData, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $isDetail ? __('Detail Pegawai') : __('Ubah Pegawai'),
            'workUnits'       => WorkUnit::all()->pluck('work_unit_name', 'id')->toArray(),
        ]);
    }

    /**
     * get export data
     *
     * @return array
     */
    protected function getExportData(): array
    {
        $times = date('Y-m-d_H-i-s');
        $data = [
            'isExport'   => true,
            'pdf_name'   => $times . '_pegawai.pdf',
            'excel_name' => $times . '_pegawai.xlsx',
            'csv_name'   => $times . '_pegawai.csv',
            'json_name'  => $times . '_pegawai.json',
        ];
        return array_merge($this->getIndexData(), $data);
    }

    /**
     * showing crud example page
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->getIndexData();

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'data'    => view('stisla.employees.table', $data)->render(),
            ]);
        }

        return view('stisla.employees.index', $data);
    }

    /**
     * datatable yajra index
     *
     * @return Response
     */
    public function yajraAjax()
    {
        $defaultData = $this->getDefaultDataIndex(__('Pegawai'), 'Pegawai', 'employees');
        return $this->employeeRepository->getYajraDataTables($defaultData);
    }

    /**
     * showing add new crud example page
     *
     * @return Response
     */
    public function create()
    {
        $title      = __('Pegawai');
        $fullTitle  = __('Tambah Pegawai');
        $data       = $this->getDefaultDataCreate($title, 'employees');
        $data       = array_merge($data, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $fullTitle,
            'workUnits'       => WorkUnit::all()->pluck('work_unit_name', 'id')->toArray(),
        ]);
        if (request()->ajax()) {
            return view('stisla.employees.only-form', $data);
        }
        return view('stisla.employees.form', $data);
    }

    /**
     * save new crud example to db
     *
     * @param EmployeeRequest $request
     * @return Response
     */
    public function store(EmployeeRequest $request)
    {
        $data   = $this->getStoreData($request);
        $result = $this->employeeRepository->create($data);
        logCreate("Pegawai", $result);
        $successMessage = successMessageCreate("Pegawai");

        // if ($request->ajax()) {
        //     return response()->json([
        //         'success' => true,
        //         'message' => $successMessage,
        //     ]);
        // }

        // return redirect()->route('inserted', encode_id($result->id))->with('successMessage', $successMessage);

        return back()->with('successMessage', $successMessage);
    }

    /**
     * showing edit crud example page
     *
     * @param Employee $employee
     * @return Response
     */
    public function edit(Employee $employee)
    {
        $data = $this->getDetailData($employee);

        if (request()->ajax()) {
            return view('stisla.employees.only-form', $data);
        }

        return view('stisla.employees.form', $data);
    }

    /**
     * update data to db
     *
     * @param EmployeeRequest $request
     * @param Employee $employee
     * @return Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        $data    = $this->getStoreData($request);
        $newData = $this->employeeRepository->update($data, $employee->id);
        logUpdate("Pegawai", $employee, $newData);
        $successMessage = successMessageUpdate("Pegawai");

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    public function show(Employee $employee)
    {
        $data = $this->getDetailData($employee, true);

        if (request()->ajax()) {
            return view('stisla.employees.only-form', $data);
        }

        return view('stisla.employees.form', $data);
    }

    public function inserted($id)
    {
        try {
            $employee = $this->employeeRepository->find(decode_id($id));
            return view('stisla.homes.inserted', [
                'd'        => $employee,
                'disabled' => true,
                'meets'    => [],
            ]);
        } catch (Exception $e) {
            abort(404);
        }
        $data = $this->getDetailData($employee, true);

        if (request()->ajax()) {
            return view('stisla.employees.only-form', $data);
        }

        return view('stisla.employees.form', array_merge($data, [
            'disabled' => true
        ]));
    }

    /**
     * delete crud example from db
     *
     * @param Employee $employee
     * @return Response
     */
    public function destroy(Employee $employee)
    {
        // $this->fileService->deleteAttachmentEmployee($employee);
        $this->employeeRepository->delete($employee->id);
        logDelete("Pegawai", $employee);
        $successMessage = successMessageDelete("Pegawai");

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    /**
     * download import example
     *
     * @return BinaryFileResponse
     */
    public function importExcelExample(): BinaryFileResponse
    {
        // bisa gunakan file excel langsung sebagai contoh
        // $filepath = public_path('example.xlsx');
        // return response()->download($filepath);

        $excel = new EmployeeExport($this->employeeRepository->getLatest());
        return $this->fileService->downloadExcel($excel, 'crud_examples_import.xlsx');
    }

    /**
     * import excel file to db
     *
     * @param ImportExcelRequest $request
     * @return Response
     */
    public function importExcel(ImportExcelRequest $request)
    {
        $this->fileService->importExcel(new EmployeeImport, $request->file('import_file'));
        $successMessage = successMessageImportExcel("Pegawai");
        return back()->with('successMessage', $successMessage);
    }

    /**
     * download export data as json
     *
     * @return BinaryFileResponse
     */
    public function json(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadJson($data['data'], $data['json_name']);
    }

    /**
     * download export data as xlsx
     *
     * @return Response
     */
    public function excel(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadExcelGeneral('stisla.employees.table', $data, $data['excel_name']);
    }

    /**
     * download export data as csv
     *
     * @return Response
     */
    public function csv(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadCsvGeneral('stisla.employees.table', $data, $data['csv_name']);
    }

    /**
     * download export data as pdf
     *
     * @return Response
     */
    public function pdf(): Response
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadPdfA4('stisla.includes.others.export-pdf', $data, $data['pdf_name']);
    }

    public function graphic()
    {
        if (request('_token'))
            $data = $this->employeeRepository->getFullData();
        else
            $data = collect([]);
        $data = $data->reverse()->values();
        $values = $data->groupBy('date')->map(function ($item) {
            return $item->count();
        })->values()->toArray();
        $labels = $data->pluck('date')->unique()->values()->toArray();
        return view('stisla.employees.graphic', [
            'values' => $values,
            'labels' => $labels,
        ]);
    }
}
