<?php

namespace App\Http\Controllers;

use App\Exports\WorkUnitExport;
use App\Http\Requests\WorkUnitRequest;
use App\Http\Requests\ImportExcelRequest;
use App\Imports\WorkUnitImport;
use App\Models\WorkUnit;
use App\Repositories\WorkUnitRepository;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class WorkUnitController extends StislaController
{

    /**
     * crud example repository
     *
     * @var WorkUnitRepository
     */
    private WorkUnitRepository $workUnitRepository;

    /**
     * constructor method
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->icon                  = 'fa fa-university';
        $this->workUnitRepository = new WorkUnitRepository;

        // $this->defaultMiddleware('Unit Kerja');
        $this->middleware('can:Unit Kerja')->only(['index', 'indexYajra', 'indexAjax', 'indexAjaxYajra']);
    }

    /**
     * get index data
     *
     * @return array
     */
    protected function getIndexData()
    {
        $isYajra = Route::is('work-units.index-yajra');
        $isAjax  = Route::is('work-units.index-ajax');
        $isAjaxYajra = Route::is('work-units.index-ajax-yajra');
        if ($isYajra || $isAjaxYajra) {
            $data = collect([]);
        } else {
            // if (request('_token'))
            $data = $this->workUnitRepository->getLatest();
            // else
            //     $data = collect([]);
        }

        $defaultData = $this->getDefaultDataIndex(__('Unit Kerja'), 'Unit Kerja', 'work-units');
        return array_merge($defaultData, [
            'data'         => $data,
            'isYajra'      => $isYajra,
            'isAjax'       => $isAjax,
            'isAjaxYajra'  => $isAjaxYajra,
            'yajraColumns' => $this->workUnitRepository->getYajraColumns(),
        ]);
    }

    /**
     * prepare store data
     *
     * @param WorkUnitRequest $request
     * @return array
     */
    private function getStoreData(WorkUnitRequest $request)
    {
        $data = $request->only([
            'work_unit_name',
        ]);
        // $base64 = $request->photo;
        // $pathToSave = storage_path('app/public/work-units/attachments/' . $request->full_name . date('YmdHis') . '.jpg');
        // $pathToSave = $this->fileService->base64ToJpeg($base64, 'work-units/attachments/');
        // $data['attachment'] = $pathToSave;
        // if ($request->hasFile('attachment')) {
        //     $data['attachment'] = $this->fileService->uploadAttachmentWorkUnit($request->file('attachment'));
        // }
        // $data['currency'] = str_replace(',', '', $request->currency);
        // $data['currency_idr'] = str_replace('.', '', $request->currency_idr);

        return $data;
    }

    /**
     * get detail data
     *
     * @param WorkUnit $workUnit
     * @param bool $isDetail
     * @return array
     */
    private function getDetailData(WorkUnit $workUnit, bool $isDetail = false)
    {
        $title       = __('Unit Kerja');
        $defaultData = $this->getDefaultDataDetail($title, 'work-units', $workUnit, $isDetail);
        return array_merge($defaultData, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $isDetail ? __('Detail Unit Kerja') : __('Ubah Unit Kerja'),
        ]);
    }

    /**
     * get export data
     *
     * @return array
     */
    protected function getExportData(): array
    {
        $times = date('Y-m-d_H-i-s');
        $data = [
            'isExport'   => true,
            'pdf_name'   => $times . '_unit_kerja.pdf',
            'excel_name' => $times . '_unit_kerja.xlsx',
            'csv_name'   => $times . '_unit_kerja.csv',
            'json_name'  => $times . '_unit_kerja.json',
        ];
        return array_merge($this->getIndexData(), $data);
    }

    /**
     * showing crud example page
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->getIndexData();

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'data'    => view('stisla.work-units.table', $data)->render(),
            ]);
        }

        return view('stisla.work-units.index', $data);
    }

    /**
     * datatable yajra index
     *
     * @return Response
     */
    public function yajraAjax()
    {
        $defaultData = $this->getDefaultDataIndex(__('Unit Kerja'), 'Unit Kerja', 'work-units');
        return $this->workUnitRepository->getYajraDataTables($defaultData);
    }

    /**
     * showing add new crud example page
     *
     * @return Response
     */
    public function create()
    {
        $title      = __('Unit Kerja');
        $fullTitle  = __('Tambah Unit Kerja');
        $data       = $this->getDefaultDataCreate($title, 'work-units');
        $data       = array_merge($data, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $fullTitle,
        ]);
        if (request()->ajax()) {
            return view('stisla.work-units.only-form', $data);
        }
        return view('stisla.work-units.form', $data);
    }

    /**
     * save new crud example to db
     *
     * @param WorkUnitRequest $request
     * @return Response
     */
    public function store(WorkUnitRequest $request)
    {
        $data   = $this->getStoreData($request);
        $result = $this->workUnitRepository->create($data);
        logCreate("Unit Kerja", $result);
        $successMessage = successMessageCreate("Unit Kerja");

        // if ($request->ajax()) {
        //     return response()->json([
        //         'success' => true,
        //         'message' => $successMessage,
        //     ]);
        // }

        // return redirect()->route('inserted', encode_id($result->id))->with('successMessage', $successMessage);

        return back()->with('successMessage', $successMessage);
    }

    /**
     * showing edit crud example page
     *
     * @param WorkUnit $workUnit
     * @return Response
     */
    public function edit(WorkUnit $workUnit)
    {
        $data = $this->getDetailData($workUnit);

        if (request()->ajax()) {
            return view('stisla.work-units.only-form', $data);
        }

        return view('stisla.work-units.form', $data);
    }

    /**
     * update data to db
     *
     * @param WorkUnitRequest $request
     * @param WorkUnit $workUnit
     * @return Response
     */
    public function update(WorkUnitRequest $request, WorkUnit $workUnit)
    {
        $data    = $this->getStoreData($request);
        $newData = $this->workUnitRepository->update($data, $workUnit->id);
        logUpdate("Unit Kerja", $workUnit, $newData);
        $successMessage = successMessageUpdate("Unit Kerja");

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    public function show(WorkUnit $workUnit)
    {
        $data = $this->getDetailData($workUnit, true);

        if (request()->ajax()) {
            return view('stisla.work-units.only-form', $data);
        }

        return view('stisla.work-units.form', $data);
    }

    public function inserted($id)
    {
        try {
            $workUnit = $this->workUnitRepository->find(decode_id($id));
            return view('stisla.homes.inserted', [
                'd'        => $workUnit,
                'disabled' => true,
                'meets'    => [],
            ]);
        } catch (Exception $e) {
            abort(404);
        }
        $data = $this->getDetailData($workUnit, true);

        if (request()->ajax()) {
            return view('stisla.work-units.only-form', $data);
        }

        return view('stisla.work-units.form', array_merge($data, [
            'disabled' => true
        ]));
    }

    /**
     * delete crud example from db
     *
     * @param WorkUnit $workUnit
     * @return Response
     */
    public function destroy(WorkUnit $workUnit)
    {
        // $this->fileService->deleteAttachmentWorkUnit($workUnit);
        $this->workUnitRepository->delete($workUnit->id);
        logDelete("Unit Kerja", $workUnit);
        $successMessage = successMessageDelete("Unit Kerja");

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    /**
     * download import example
     *
     * @return BinaryFileResponse
     */
    public function importExcelExample(): BinaryFileResponse
    {
        // bisa gunakan file excel langsung sebagai contoh
        // $filepath = public_path('example.xlsx');
        // return response()->download($filepath);

        $excel = new WorkUnitExport($this->workUnitRepository->getLatest());
        return $this->fileService->downloadExcel($excel, 'crud_examples_import.xlsx');
    }

    /**
     * import excel file to db
     *
     * @param ImportExcelRequest $request
     * @return Response
     */
    public function importExcel(ImportExcelRequest $request)
    {
        $this->fileService->importExcel(new WorkUnitImport, $request->file('import_file'));
        $successMessage = successMessageImportExcel("Unit Kerja");
        return back()->with('successMessage', $successMessage);
    }

    /**
     * download export data as json
     *
     * @return BinaryFileResponse
     */
    public function json(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadJson($data['data'], $data['json_name']);
    }

    /**
     * download export data as xlsx
     *
     * @return Response
     */
    public function excel(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadExcelGeneral('stisla.work-units.table', $data, $data['excel_name']);
    }

    /**
     * download export data as csv
     *
     * @return Response
     */
    public function csv(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadCsvGeneral('stisla.work-units.table', $data, $data['csv_name']);
    }

    /**
     * download export data as pdf
     *
     * @return Response
     */
    public function pdf(): Response
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadPdfA4('stisla.includes.others.export-pdf', $data, $data['pdf_name']);
    }

    public function graphic()
    {
        if (request('_token'))
            $data = $this->workUnitRepository->getFullData();
        else
            $data = collect([]);
        $data = $data->reverse()->values();
        $values = $data->groupBy('date')->map(function ($item) {
            return $item->count();
        })->values()->toArray();
        $labels = $data->pluck('date')->unique()->values()->toArray();
        return view('stisla.work-units.graphic', [
            'values' => $values,
            'labels' => $labels,
        ]);
    }
}
