<?php

namespace App\Http\Controllers;

use App\Exports\SliderExport;
use App\Http\Requests\SliderRequest;
use App\Http\Requests\ImportExcelRequest;
use App\Imports\SliderImport;
use App\Models\Slider;
use App\Repositories\SliderRepository;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class SliderController extends StislaController
{

    /**
     * crud example repository
     *
     * @var SliderRepository
     */
    private SliderRepository $sliderRepository;

    /**
     * constructor method
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->icon                  = 'fa fa-chart-line';
        $this->sliderRepository = new SliderRepository;

        // $this->defaultMiddleware('Slider');
        $this->middleware('can:Slider')->only(['index', 'indexYajra', 'indexAjax', 'indexAjaxYajra']);
    }

    /**
     * get index data
     *
     * @return array
     */
    protected function getIndexData()
    {
        $isYajra = Route::is('sliders.index-yajra');
        $isAjax  = Route::is('sliders.index-ajax');
        $isAjaxYajra = Route::is('sliders.index-ajax-yajra');
        if ($isYajra || $isAjaxYajra) {
            $data = collect([]);
        } else {
            // if (request('_token'))
            $data = $this->sliderRepository->getLatest();
            // else
            //     $data = collect([]);
        }

        $defaultData = $this->getDefaultDataIndex(__('Slider'), 'Slider', 'sliders');
        return array_merge($defaultData, [
            'data'         => $data,
            'isYajra'      => $isYajra,
            'isAjax'       => $isAjax,
            'isAjaxYajra'  => $isAjaxYajra,
            'yajraColumns' => $this->sliderRepository->getYajraColumns(),
        ]);
    }

    /**
     * prepare store data
     *
     * @param SliderRequest $request
     * @return array
     */
    private function getStoreData(SliderRequest $request)
    {
        $data = $request->only([
            'text',
        ]);
        // $base64 = $request->photo;
        // $pathToSave = storage_path('app/public/sliders/attachments/' . $request->full_name . date('YmdHis') . '.jpg');
        // $pathToSave = $this->fileService->base64ToJpeg($base64, 'sliders/attachments/');
        // $data['attachment'] = $pathToSave;
        // if ($request->hasFile('attachment')) {
        //     $data['attachment'] = $this->fileService->uploadAttachmentSlider($request->file('attachment'));
        // }
        // $data['currency'] = str_replace(',', '', $request->currency);
        // $data['currency_idr'] = str_replace('.', '', $request->currency_idr);

        return $data;
    }

    /**
     * get detail data
     *
     * @param Slider $slider
     * @param bool $isDetail
     * @return array
     */
    private function getDetailData(Slider $slider, bool $isDetail = false)
    {
        $title       = __('Slider');
        $defaultData = $this->getDefaultDataDetail($title, 'sliders', $slider, $isDetail);
        return array_merge($defaultData, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $isDetail ? __('Detail Slider') : __('Ubah Slider'),
        ]);
    }

    /**
     * get export data
     *
     * @return array
     */
    protected function getExportData(): array
    {
        $times = date('Y-m-d_H-i-s');
        $data = [
            'isExport'   => true,
            'pdf_name'   => $times . '_slider.pdf',
            'excel_name' => $times . '_slider.xlsx',
            'csv_name'   => $times . '_slider.csv',
            'json_name'  => $times . '_slider.json',
        ];
        return array_merge($this->getIndexData(), $data);
    }

    /**
     * showing crud example page
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->getIndexData();

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'data'    => view('stisla.sliders.table', $data)->render(),
            ]);
        }

        return view('stisla.sliders.index', $data);
    }

    /**
     * datatable yajra index
     *
     * @return Response
     */
    public function yajraAjax()
    {
        $defaultData = $this->getDefaultDataIndex(__('Slider'), 'Slider', 'sliders');
        return $this->sliderRepository->getYajraDataTables($defaultData);
    }

    /**
     * showing add new crud example page
     *
     * @return Response
     */
    public function create()
    {
        $title      = __('Slider');
        $fullTitle  = __('Tambah Slider');
        $data       = $this->getDefaultDataCreate($title, 'sliders');
        $data       = array_merge($data, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $fullTitle,
        ]);
        if (request()->ajax()) {
            return view('stisla.sliders.only-form', $data);
        }
        return view('stisla.sliders.form', $data);
    }

    /**
     * save new crud example to db
     *
     * @param SliderRequest $request
     * @return Response
     */
    public function store(SliderRequest $request)
    {
        $data   = $this->getStoreData($request);
        $result = $this->sliderRepository->create($data);
        logCreate("Slider", $result);
        $successMessage = successMessageCreate("Slider");

        // if ($request->ajax()) {
        //     return response()->json([
        //         'success' => true,
        //         'message' => $successMessage,
        //     ]);
        // }

        // return redirect()->route('inserted', encode_id($result->id))->with('successMessage', $successMessage);

        return back()->with('successMessage', $successMessage);
    }

    /**
     * showing edit crud example page
     *
     * @param Slider $slider
     * @return Response
     */
    public function edit(Slider $slider)
    {
        $data = $this->getDetailData($slider);

        if (request()->ajax()) {
            return view('stisla.sliders.only-form', $data);
        }

        return view('stisla.sliders.form', $data);
    }

    /**
     * update data to db
     *
     * @param SliderRequest $request
     * @param Slider $slider
     * @return Response
     */
    public function update(SliderRequest $request, Slider $slider)
    {
        $data    = $this->getStoreData($request);
        $newData = $this->sliderRepository->update($data, $slider->id);
        logUpdate("Slider", $slider, $newData);
        $successMessage = successMessageUpdate("Slider");

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    public function show(Slider $slider)
    {
        $data = $this->getDetailData($slider, true);

        if (request()->ajax()) {
            return view('stisla.sliders.only-form', $data);
        }

        return view('stisla.sliders.form', $data);
    }

    public function inserted($id)
    {
        try {
            $slider = $this->sliderRepository->find(decode_id($id));
            return view('stisla.homes.inserted', [
                'd'        => $slider,
                'disabled' => true,
                'meets'    => [],
            ]);
        } catch (Exception $e) {
            abort(404);
        }
        $data = $this->getDetailData($slider, true);

        if (request()->ajax()) {
            return view('stisla.sliders.only-form', $data);
        }

        return view('stisla.sliders.form', array_merge($data, [
            'disabled' => true
        ]));
    }

    /**
     * delete crud example from db
     *
     * @param Slider $slider
     * @return Response
     */
    public function destroy(Slider $slider)
    {
        // $this->fileService->deleteAttachmentSlider($slider);
        $this->sliderRepository->delete($slider->id);
        logDelete("Slider", $slider);
        $successMessage = successMessageDelete("Slider");

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    /**
     * download import example
     *
     * @return BinaryFileResponse
     */
    public function importExcelExample(): BinaryFileResponse
    {
        // bisa gunakan file excel langsung sebagai contoh
        // $filepath = public_path('example.xlsx');
        // return response()->download($filepath);

        $excel = new SliderExport($this->sliderRepository->getLatest());
        return $this->fileService->downloadExcel($excel, 'crud_examples_import.xlsx');
    }

    /**
     * import excel file to db
     *
     * @param ImportExcelRequest $request
     * @return Response
     */
    public function importExcel(ImportExcelRequest $request)
    {
        $this->fileService->importExcel(new SliderImport, $request->file('import_file'));
        $successMessage = successMessageImportExcel("Slider");
        return back()->with('successMessage', $successMessage);
    }

    /**
     * download export data as json
     *
     * @return BinaryFileResponse
     */
    public function json(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadJson($data['data'], $data['json_name']);
    }

    /**
     * download export data as xlsx
     *
     * @return Response
     */
    public function excel(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadExcelGeneral('stisla.sliders.table', $data, $data['excel_name']);
    }

    /**
     * download export data as csv
     *
     * @return Response
     */
    public function csv(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadCsvGeneral('stisla.sliders.table', $data, $data['csv_name']);
    }

    /**
     * download export data as pdf
     *
     * @return Response
     */
    public function pdf(): Response
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadPdfA4('stisla.includes.others.export-pdf', $data, $data['pdf_name']);
    }

    public function graphic()
    {
        if (request('_token'))
            $data = $this->sliderRepository->getFullData();
        else
            $data = collect([]);
        $data = $data->reverse()->values();
        $values = $data->groupBy('date')->map(function ($item) {
            return $item->count();
        })->values()->toArray();
        $labels = $data->pluck('date')->unique()->values()->toArray();
        return view('stisla.sliders.graphic', [
            'values' => $values,
            'labels' => $labels,
        ]);
    }
}
