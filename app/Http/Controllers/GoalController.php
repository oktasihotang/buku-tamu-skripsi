<?php

namespace App\Http\Controllers;

use App\Exports\GoalExport;
use App\Http\Requests\GoalRequest;
use App\Http\Requests\ImportExcelRequest;
use App\Imports\GoalImport;
use App\Models\Goal;
use App\Repositories\GoalRepository;
use Exception;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class GoalController extends StislaController
{

    /**
     * crud example repository
     *
     * @var GoalRepository
     */
    private GoalRepository $goalRepository;

    /**
     * constructor method
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->icon                  = 'fa fa-bookmark';
        $this->goalRepository = new GoalRepository;

        // $this->defaultMiddleware('Goal');
        $this->middleware('can:Keperluan')->only(['index', 'indexYajra', 'indexAjax', 'indexAjaxYajra']);
    }

    /**
     * get index data
     *
     * @return array
     */
    protected function getIndexData()
    {
        $isYajra = Route::is('goals.index-yajra');
        $isAjax  = Route::is('goals.index-ajax');
        $isAjaxYajra = Route::is('goals.index-ajax-yajra');
        if ($isYajra || $isAjaxYajra) {
            $data = collect([]);
        } else {
            // if (request('_token'))
            $data = $this->goalRepository->getLatest();
            // else
            //     $data = collect([]);
        }

        $defaultData = $this->getDefaultDataIndex(__('Keperluan'), 'Keperluan', 'goals');
        return array_merge($defaultData, [
            'data'         => $data,
            'isYajra'      => $isYajra,
            'isAjax'       => $isAjax,
            'isAjaxYajra'  => $isAjaxYajra,
            'yajraColumns' => $this->goalRepository->getYajraColumns(),
        ]);
    }

    /**
     * prepare store data
     *
     * @param GoalRequest $request
     * @return array
     */
    private function getStoreData(GoalRequest $request)
    {
        $data = $request->only([
            'goal_name',
        ]);
        // $base64 = $request->photo;
        // $pathToSave = storage_path('app/public/goals/attachments/' . $request->full_name . date('YmdHis') . '.jpg');
        // $pathToSave = $this->fileService->base64ToJpeg($base64, 'goals/attachments/');
        // $data['attachment'] = $pathToSave;
        // if ($request->hasFile('attachment')) {
        //     $data['attachment'] = $this->fileService->uploadAttachmentGoal($request->file('attachment'));
        // }
        // $data['currency'] = str_replace(',', '', $request->currency);
        // $data['currency_idr'] = str_replace('.', '', $request->currency_idr);

        return $data;
    }

    /**
     * get detail data
     *
     * @param Goal $goal
     * @param bool $isDetail
     * @return array
     */
    private function getDetailData(Goal $goal, bool $isDetail = false)
    {
        $title       = __('Keperluan');
        $defaultData = $this->getDefaultDataDetail($title, 'goals', $goal, $isDetail);
        return array_merge($defaultData, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $isDetail ? __('Detail Keperluan') : __('Ubah Keperluan'),
        ]);
    }

    /**
     * get export data
     *
     * @return array
     */
    protected function getExportData(): array
    {
        $times = date('Y-m-d_H-i-s');
        $data = [
            'isExport'   => true,
            'pdf_name'   => $times . '_keperluan.pdf',
            'excel_name' => $times . '_keperluan.xlsx',
            'csv_name'   => $times . '_keperluan.csv',
            'json_name'  => $times . '_keperluan.json',
        ];
        return array_merge($this->getIndexData(), $data);
    }

    /**
     * showing crud example page
     *
     * @return Response
     */
    public function index()
    {
        $data = $this->getIndexData();

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'data'    => view('stisla.goals.table', $data)->render(),
            ]);
        }

        return view('stisla.goals.index', $data);
    }

    /**
     * datatable yajra index
     *
     * @return Response
     */
    public function yajraAjax()
    {
        $defaultData = $this->getDefaultDataIndex(__('Goal'), 'Goal', 'goals');
        return $this->goalRepository->getYajraDataTables($defaultData);
    }

    /**
     * showing add new crud example page
     *
     * @return Response
     */
    public function create()
    {
        $title      = __('Keperluan');
        $fullTitle  = __('Tambah Keperluan');
        $data       = $this->getDefaultDataCreate($title, 'goals');
        $data       = array_merge($data, [
            'selectOptions'   => get_options(10),
            'radioOptions'    => get_options(4),
            'checkboxOptions' => get_options(5),
            'fullTitle'       => $fullTitle,
        ]);
        if (request()->ajax()) {
            return view('stisla.goals.only-form', $data);
        }
        return view('stisla.goals.form', $data);
    }

    /**
     * save new crud example to db
     *
     * @param GoalRequest $request
     * @return Response
     */
    public function store(GoalRequest $request)
    {
        $data   = $this->getStoreData($request);
        $result = $this->goalRepository->create($data);
        logCreate("Goal", $result);
        $successMessage = successMessageCreate("Keperluan");

        // if ($request->ajax()) {
        //     return response()->json([
        //         'success' => true,
        //         'message' => $successMessage,
        //     ]);
        // }

        // return redirect()->route('inserted', encode_id($result->id))->with('successMessage', $successMessage);

        return back()->with('successMessage', $successMessage);
    }

    /**
     * showing edit crud example page
     *
     * @param Goal $goal
     * @return Response
     */
    public function edit(Goal $goal)
    {
        $data = $this->getDetailData($goal);

        if (request()->ajax()) {
            return view('stisla.goals.only-form', $data);
        }

        return view('stisla.goals.form', $data);
    }

    /**
     * update data to db
     *
     * @param GoalRequest $request
     * @param Goal $goal
     * @return Response
     */
    public function update(GoalRequest $request, Goal $goal)
    {
        $data    = $this->getStoreData($request);
        $newData = $this->goalRepository->update($data, $goal->id);
        logUpdate("Keperluan", $goal, $newData);
        $successMessage = successMessageUpdate("Keperluan");

        if ($request->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    public function show(Goal $goal)
    {
        $data = $this->getDetailData($goal, true);

        if (request()->ajax()) {
            return view('stisla.goals.only-form', $data);
        }

        return view('stisla.goals.form', $data);
    }

    public function inserted($id)
    {
        try {
            $goal = $this->goalRepository->find(decode_id($id));
            return view('stisla.homes.inserted', [
                'd'        => $goal,
                'disabled' => true,
                'meets'    => [],
            ]);
        } catch (Exception $e) {
            abort(404);
        }
        $data = $this->getDetailData($goal, true);

        if (request()->ajax()) {
            return view('stisla.goals.only-form', $data);
        }

        return view('stisla.goals.form', array_merge($data, [
            'disabled' => true
        ]));
    }

    /**
     * delete crud example from db
     *
     * @param Goal $goal
     * @return Response
     */
    public function destroy(Goal $goal)
    {
        // $this->fileService->deleteAttachmentGoal($goal);
        $this->goalRepository->delete($goal->id);
        logDelete("Keperluan", $goal);
        $successMessage = successMessageDelete("Keperluan");

        if (request()->ajax()) {
            return response()->json([
                'success' => true,
                'message' => $successMessage,
            ]);
        }

        return back()->with('successMessage', $successMessage);
    }

    /**
     * download import example
     *
     * @return BinaryFileResponse
     */
    public function importExcelExample(): BinaryFileResponse
    {
        // bisa gunakan file excel langsung sebagai contoh
        // $filepath = public_path('example.xlsx');
        // return response()->download($filepath);

        $excel = new GoalExport($this->goalRepository->getLatest());
        return $this->fileService->downloadExcel($excel, 'crud_examples_import.xlsx');
    }

    /**
     * import excel file to db
     *
     * @param ImportExcelRequest $request
     * @return Response
     */
    public function importExcel(ImportExcelRequest $request)
    {
        $this->fileService->importExcel(new GoalImport, $request->file('import_file'));
        $successMessage = successMessageImportExcel("Goal");
        return back()->with('successMessage', $successMessage);
    }

    /**
     * download export data as json
     *
     * @return BinaryFileResponse
     */
    public function json(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadJson($data['data'], $data['json_name']);
    }

    /**
     * download export data as xlsx
     *
     * @return Response
     */
    public function excel(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadExcelGeneral('stisla.goals.table', $data, $data['excel_name']);
    }

    /**
     * download export data as csv
     *
     * @return Response
     */
    public function csv(): BinaryFileResponse
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadCsvGeneral('stisla.goals.table', $data, $data['csv_name']);
    }

    /**
     * download export data as pdf
     *
     * @return Response
     */
    public function pdf(): Response
    {
        $data  = $this->getExportData();
        return $this->fileService->downloadPdfA4('stisla.includes.others.export-pdf', $data, $data['pdf_name']);
    }

    public function graphic()
    {
        if (request('_token'))
            $data = $this->goalRepository->getFullData();
        else
            $data = collect([]);
        $data = $data->reverse()->values();
        $values = $data->groupBy('date')->map(function ($item) {
            return $item->count();
        })->values()->toArray();
        $labels = $data->pluck('date')->unique()->values()->toArray();
        return view('stisla.goals.graphic', [
            'values' => $values,
            'labels' => $labels,
        ]);
    }
}
